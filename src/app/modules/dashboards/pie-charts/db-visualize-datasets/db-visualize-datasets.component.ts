import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-db-visualize-datasets',
  templateUrl: './db-visualize-datasets.component.html',
  styleUrls: ['./db-visualize-datasets.component.scss']
})
export class DbVisualizeDatasetsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  graph1 = {
    data : [{
      values: [40, 31, 80],
      labels: ['Dataset A', 'Dataset B', 'Dataset C'],
      type: 'pie',
      textinfo: 'label',
      marker: {
        colors: ['rgb(43, 42, 162)', 'rgb(122, 55, 97)', 'rgb(63, 194, 147)'],
        line:{
          color: 'black',
          width: 1
        }
      },
    }],
    layout : {
      showlegend: false
    },
    config: {
      resposive: 'true'
    }
  }
}
