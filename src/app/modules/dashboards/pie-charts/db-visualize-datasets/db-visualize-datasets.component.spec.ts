import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DbVisualizeDatasetsComponent } from './db-visualize-datasets.component';

describe('DbVisualizeDatasetsComponent', () => {
  let component: DbVisualizeDatasetsComponent;
  let fixture: ComponentFixture<DbVisualizeDatasetsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DbVisualizeDatasetsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DbVisualizeDatasetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
