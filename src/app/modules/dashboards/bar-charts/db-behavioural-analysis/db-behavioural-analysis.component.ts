import { Component, Input, OnInit } from '@angular/core';
import { HttpService } from '../../../../services/http.service'
import { ChangeDetectorRef, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-db-behavioural-analysis',
  templateUrl: './db-behavioural-analysis.component.html',
  styleUrls: ['./db-behavioural-analysis.component.scss']
})
export class DbBehaviouralAnalysisComponent implements OnInit {

  @Input() receivedData: Map<string, number>;

  ngOnChanges(changes: SimpleChanges) {
    if (changes['receivedData']) {
      this.changeGraph()
    }
}

  data = new Map<string, number>();


  constructor(private http: HttpService, private cd: ChangeDetectorRef) { }
  graph1 = {
    data: {},
    layout: {}
  }


  ngOnInit(): void {
    this.initGraph()

    /* WHEN API IS REQUIRED */
    // this.http.retrieveData('qsdy').subscribe(res => {
    //   for(let i = 0; i< res['data'].length;i++){
    //     this.data.set(res['data'][i]['Activity'], this.data.get(res['data'][i]['Activity'])+1);
    //   }
    //   console.log(res['data'])
    //   console.log(this.data)      
    // });
}

  initGraph(){
    this.graph1 = {
      data: [
        { x: ['Sleeping/Seating', 'Walking', 'Running', 'In a vehicle', 'Exercise'], 
          y: [this.receivedData.get('sleep'), this.receivedData.get('walking'), this.receivedData.get('running'), this.receivedData.get('vehicle'), this.receivedData.get('exercise')], 
          type: 'bar',
          marker: {
            color: 'rgb(59, 112, 128)',
            line: {
              color: 'rgb(26, 49, 56)',
              width: 1.5
            }
          }
        },
      ],
      layout : {
        xaxis: {title: 'Activity'},
        yaxis: {title: 'Hours'}
      }
    };
  }

  changeGraph(){
    this.graph1.data[0].y = [this.receivedData.get('sleep'), this.receivedData.get('walking'), this.receivedData.get('running'), this.receivedData.get('vehicle'), this.receivedData.get('exercise')];
  }
  
  

}
