import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DbBehaviouralAnalysisComponent } from './db-behavioural-analysis.component';

describe('DbBehaviouralAnalysisComponent', () => {
  let component: DbBehaviouralAnalysisComponent;
  let fixture: ComponentFixture<DbBehaviouralAnalysisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DbBehaviouralAnalysisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DbBehaviouralAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
