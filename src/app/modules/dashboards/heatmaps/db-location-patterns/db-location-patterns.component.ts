import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { HttpService } from '../../../../services/http.service'
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-db-location-patterns',
  templateUrl: './db-location-patterns.component.html',
  styleUrls: ['./db-location-patterns.component.scss']
})
export class DbLocationPatternsComponent implements OnInit {
 
  @Input() receivedData: any;

  latitudes = []
  longitudes = []
  z_values = []
  graph1 = {
    config: {},
    layout: {},
    data: {},
  }
  data = new Map<string, number>();

  constructor(private http: HttpService, private cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    /* WHEN API IS REQUIRED */
    // this.retrieveData();
    this.changeGraph();
  }

  // retrieveData(){
  //  this.http.retrieveData('qsdy').subscribe(res => {
  //   for(let i = 0; i< 500;i++){
  //     let a = res['data'][i]['Geo'].replace(/[{( ')}]/g,'').split(",") 
  //     // Get coords
  //     this.latitudes.push(a[0])    
  //     this.longitudes.push(a[1])
  //     this.z_values.push(Math.floor(Math.random() * 6) + 1  )    
  //   }
  //   // 
  //   this.changeGraph()
  //   this.showPlot=true;
  //   this.cd.detectChanges();
  //   });
  // }


  changeGraph(){
    // Parse the received Data
    for(let i = 0; i<this.receivedData.length; i++){
      this.latitudes.push(this.receivedData[i].latitude);
      this.longitudes.push(this.receivedData[i].longitude);
      this.z_values.push(1)
    }
    const latitude_sum = this.latitudes.reduce((a, b) => a + b, 0)
    const longitude_sum = this.longitudes.reduce((a, b) => a + b, 0)
    
    this.graph1 = {
      data: [
        {
          type: "densitymapbox", 
          lon: this.longitudes, 
          lat: this.latitudes, 
          coloraxis: 'coloraxis',
          z: this.z_values,
          radius: 20, 
          },
      ],
       layout: {
        coloraxis: {
          colorscale: "YlGnBu"
        },
         mapbox: {
           style: 'stamen-terrain', 
           center: {
            //  lat: 41.3995659,
            //  lon: 2.1865365,
             lat: (latitude_sum / this.latitudes.length),
             lon: (longitude_sum / this.longitudes.length),
            },
            zoom: 5.5
          }, 
            margin: {
                l: 0,
                r: 0,
                b: 0,
                t: 0,
                pad: 2
            }
        },
        config: {
        }
    };
  }
}
