import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DbLocationPatternsComponent } from './db-location-patterns.component';

describe('DbLocationPatternsComponent', () => {
  let component: DbLocationPatternsComponent;
  let fixture: ComponentFixture<DbLocationPatternsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DbLocationPatternsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DbLocationPatternsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
