import { summaryFileName } from '@angular/compiler/src/aot/util';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss']
})
export class DatePickerComponent implements OnInit {

  @Output() onDatePicked = new EventEmitter<any>();

  constructor() { }
  
  ngOnInit(): void {
  }

  generateRandValues(){
    let values = [12, 4, 2, 3, 3];

    for(let i = 0; i< 6; i++) {
      let added = false;
      while(!added){
        if(i < 3){
          values[Math.floor(Math.random() * 5)] += 2;
          added = true;
        }
        else{
          const num = Math.floor(Math.random() * 5)
          if(values[num] >= 2){
            values[num] -= 2;
            added = true
          }
        }
      }
    }
    return values

  }

  changeChart() {
    this.onDatePicked.emit(this.generateRandValues());
  }
}
