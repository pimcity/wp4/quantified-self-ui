import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  
  constructor(private http: HttpClient) { }
  // // POST: Upload file method
  // uploadFile(file: any): Observable<any> {
  //   const formData: FormData = new FormData();
  //   formData.append('file', file, file.name);
  //   return this.http.post(environment.apiUrl + '/upload', formData);
  // }

  // GET: get data
  retrieveData(dataset: string) {
    return this.http.get(environment.API_URL + '/data/' + dataset, this.httpOptions);
  }
}