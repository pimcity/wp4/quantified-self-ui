import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationPatternsComponent } from './location-patterns.component';

describe('LocationPatternsComponent', () => {
  let component: LocationPatternsComponent;
  let fixture: ComponentFixture<LocationPatternsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocationPatternsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationPatternsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
