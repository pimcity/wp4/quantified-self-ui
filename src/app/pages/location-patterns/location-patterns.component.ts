import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TripDetailsComponent } from 'src/app/components/trip-details/trip-details.component';

@Component({
  selector: 'app-location-patterns',
  templateUrl: './location-patterns.component.html',
  styleUrls: ['./location-patterns.component.scss']
})
export class LocationPatternsComponent implements OnInit {

  receivedData = [
    {
      'latitude': 41.3995659,
      'longitude': 2.1865365,
      'city': 'Barcelona',
      'country': 'Spain',
      'timestamp': 'Wed Aug 25 2021 18:25:24'
    },
    {
      'latitude': 41.389059,
      'longitude': 2.1344966,
      'city': 'Barcelona',
      'country': 'Spain',
      'timestamp': 'Wed Aug 25 2021 13:10:26'
    },
    {
      'latitude': 41.3817784,
      'longitude': 2.1923269,
      'city': 'Barcelona',
      'country': 'Spain',
      'timestamp': 'Wed Aug 25 2021 12:40:22'
    },
    {
      'latitude': 41.3861954,
      'longitude': 2.1400307,
      'city': 'Barcelona',
      'country': 'Spain',
      'timestamp': 'Wed Aug 25 2021 11:59:25'
    },
    {
      'latitude': 43.60426,
      'longitude': 1.44367,
      'city': 'Toulousse',
      'country': 'France',
      'timestamp': 'Wed Aug 22 2021 12:25:25'
    },
    {
      'latitude': 43.296398,
      'longitude': 5.370000,
      'city': 'Marseille',
      'country': 'France',
      'timestamp': 'Wed Aug 21 2021 12:25:25'
    }
  ]

  tableData: any[]

  @Output()
   sendTripDetails = new EventEmitter<string>();
  
  constructor(private modalService: NgbModal) {}
  
  ngOnInit(): void {
    this.receivedData.sort(this.sortByDates)
    // Generate the table data, deleting repeated cities.
    this.tableData = this.receivedData.filter((v,i,a)=>a.findIndex(t=>(t.city === v.city))===i)
    for(let i = 0; i<this.tableData.length; i++){
      let count = 0;
      for(let j = 0; j<this.receivedData.length; j++){
        if(this.receivedData[j].city === this.tableData[i].city)
          count++;
      }
      this.tableData[i].timesBeen = count; 
    }
    // RETRIEVE DATA FROM API
    // STRUCT THE DATA into "receivedData"
  }

  openTripDetails(city){
    // Parse the data to only hold the selected city coords    
    let parsedData = this.receivedData.filter(function(row){
      return row.city === city
    }); 
    // Open modal and send the data to it
    const activeModal = this.modalService.open(TripDetailsComponent)
    activeModal.componentInstance.data = parsedData;
  }

  sortByDates(a,b){  
    var dateA = new Date(a.timestamp).getTime();
    var dateB = new Date(b.timestamp).getTime();
    return dateA < dateB ? 1 : -1;  
}; 
}
