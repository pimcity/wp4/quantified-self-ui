import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizeDatasetsComponent } from './visualize-datasets.component';

describe('VisualizeDatasetsComponent', () => {
  let component: VisualizeDatasetsComponent;
  let fixture: ComponentFixture<VisualizeDatasetsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizeDatasetsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizeDatasetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
