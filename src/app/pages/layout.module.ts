import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import * as PlotlyJS from 'plotly.js/dist/plotly.js';
import { PlotlyModule } from 'angular-plotly.js';

PlotlyModule.plotlyjs = PlotlyJS;

import { InlineSVGModule } from 'ng-inline-svg';
import { PagesRoutingModule } from './pages-routing.module';
import {
  NgbDropdownModule,
  NgbProgressbarModule,
  NgbDatepickerModule
} from '@ng-bootstrap/ng-bootstrap';
import { TranslationModule } from '../modules/i18n/translation.module';
import { LayoutComponent } from './_layout/layout.component';
import { ScriptsInitComponent } from './_layout/init/scipts-init/scripts-init.component';
import { HeaderMobileComponent } from './_layout/components/header-mobile/header-mobile.component';
import { AsideComponent } from './_layout/components/aside/aside.component';
import { FooterComponent } from './_layout/components/footer/footer.component';
import { HeaderComponent } from './_layout/components/header/header.component';
import { HeaderMenuComponent } from './_layout/components/header/header-menu/header-menu.component';
import { TopbarComponent } from './_layout/components/topbar/topbar.component';
import { ExtrasModule } from '../_metronic/partials/layout/extras/extras.module';
import { LanguageSelectorComponent } from './_layout/components/topbar/language-selector/language-selector.component';
import { CoreModule } from '../_metronic/core';
import { SubheaderModule } from '../_metronic/partials/layout/subheader/subheader.module';
import { AsideDynamicComponent } from './_layout/components/aside-dynamic/aside-dynamic.component';
import { HeaderMenuDynamicComponent } from './_layout/components/header/header-menu-dynamic/header-menu-dynamic.component';
import { PersonalDataComponent } from './personal-data/personal-data.component';
import { LocationPatternsComponent } from './location-patterns/location-patterns.component';
import { BehaviouralAnalysisComponent } from './behavioural-analysis/behavioural-analysis.component';
import { VisualizeDatasetsComponent } from './visualize-datasets/visualize-datasets.component';
import { DbBehaviouralAnalysisComponent } from '../modules/dashboards/bar-charts/db-behavioural-analysis/db-behavioural-analysis.component'
import { DbLocationPatternsComponent } from '../modules/dashboards/heatmaps/db-location-patterns/db-location-patterns.component'
import { DbVisualizeDatasetsComponent } from '../modules/dashboards/pie-charts/db-visualize-datasets/db-visualize-datasets.component'
import { DatePickerComponent } from './../components/date-picker/date-picker.component';


@NgModule({
  declarations: [
    LayoutComponent,
    ScriptsInitComponent,
    HeaderMobileComponent,
    AsideComponent,
    FooterComponent,
    HeaderComponent,
    HeaderMenuComponent,
    TopbarComponent,
    LanguageSelectorComponent,
    AsideDynamicComponent,
    HeaderMenuDynamicComponent,
    PersonalDataComponent,
    LocationPatternsComponent,
    BehaviouralAnalysisComponent,
    VisualizeDatasetsComponent,
    DbBehaviouralAnalysisComponent,
    DbLocationPatternsComponent,
    DbVisualizeDatasetsComponent,
    DatePickerComponent
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    TranslationModule,
    InlineSVGModule,
    ExtrasModule,
    NgbDropdownModule,
    NgbProgressbarModule,
    CoreModule,
    SubheaderModule,
    PlotlyModule,
    NgbDatepickerModule
    ],
})
export class LayoutModule { }
