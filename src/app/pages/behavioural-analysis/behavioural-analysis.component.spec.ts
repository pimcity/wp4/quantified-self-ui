import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BehaviouralAnalysisComponent } from './behavioural-analysis.component';

describe('BehaviouralAnalysisComponent', () => {
  let component: BehaviouralAnalysisComponent;
  let fixture: ComponentFixture<BehaviouralAnalysisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BehaviouralAnalysisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BehaviouralAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
