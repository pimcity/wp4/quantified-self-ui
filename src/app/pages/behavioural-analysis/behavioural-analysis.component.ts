import { Component, OnInit, ViewChild } from '@angular/core';
import { DatePickerComponent } from 'src/app/components/date-picker/date-picker.component';
import { DbBehaviouralAnalysisComponent } from 'src/app/modules/dashboards/bar-charts/db-behavioural-analysis/db-behavioural-analysis.component';
@Component({
  selector: 'app-behavioural-analysis',
  templateUrl: './behavioural-analysis.component.html',
  styleUrls: ['./behavioural-analysis.component.scss']
})
export class BehaviouralAnalysisComponent implements OnInit {

  receivedData = new Map<string, number>();
  @ViewChild(DbBehaviouralAnalysisComponent) DbBehaviour: DbBehaviouralAnalysisComponent;

  
  constructor() { }

  ngOnInit(): void {
    this.receivedData.set('sleep', 7);
    this.receivedData.set('walking', 2);
    this.receivedData.set('running', 8);
    this.receivedData.set('vehicle', 5);
    this.receivedData.set('exercise', 2);  
  }

  updateChart(values){
    // Fill values (Hardcoded)
    this.receivedData.set('sleep', values[0]);
    this.receivedData.set('walking', values[1]);
    this.receivedData.set('running', values[2]);
    this.receivedData.set('vehicle', values[3]);
    this.receivedData.set('exercise', values[4]);
    this.DbBehaviour.changeGraph();
  }
}
