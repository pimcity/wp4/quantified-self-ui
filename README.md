# Quantified-Self Dashboard

## Intro
The purpose of the Quantified-self dashboard is to present to the user an analysis of their behavior based on the data available, especially related to their location. Through comparing their data with other users’ aggregated data, the user will be able to better understand not only their behavior in relation to other similar users, but also to see the benefits of sharing data in the EasyPIMS setting and the value of his/her data. A simple, yet intuitive UI will guide the user to a set of basic actions regarding their profile, the data they have uploaded and the graphical representation of their behavior. 

![Architecture](docs/screenshots/architecture.png)


## Installation


### Pre-requisites

The machine running QSD must have installed:

* Angular version 11.0 or superior
* NodeJS version 15.4.0 or superior

### Deployment
Once all the prerequisites are fullfilled, run:
```
ng serve
```

Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Docker deployment

A Dockerfile and a docker-compose file have been developed for an easier deployment.

In order to run it, execute:

```
docker-compose -f .\docker-compose-dev.yml build
docker-compose -f .\docker-compose-dev.yml up -d
```
As with the non-docker deployment, to access the application navigate to `http://localhost:4200/`.

## Usage
### Location patterns
In this page you can view the different location history of the user, having a more detailed view of every location.

![Screenshot 1](docs/screenshots/screenshot_1.png)


### Behavioural analysis
The view allows the user to check the daily activities and the time spent on them, it also has a calendar to filter by day.
![Screenshot 2](docs/screenshots/screenshot_2.png)


## Changes
- Commit [dcb4f093](https://gitlab.com/pimcity/wp4/quantified-self-ui/-/commit/dcb4f0936237ed5aa169a065cd1be43da586bb75): Added calendar to Behavioural analysis page.
- Commit [3ffbe24b](https://gitlab.com/pimcity/wp4/quantified-self-ui/-/commit/3ffbe24ba5217ae3ede87aa0408bde67b7175b74): Added trip details component and updated "Location Patterns".
- Commit [1c94845b](https://gitlab.com/pimcity/wp4/quantified-self-ui/-/commit/1c94845b0c876d03fbb86fb5aabf72e21668e03b): Nginx dockers: Dev and Prod.

## License

The DPC Tool is distributed under AGPL-3.0-only, see the [LICENSE](LICENSE.txt) file.

Copyright (C) 2021 LSTech S.L - Xavi Olivares, Evangelos Kotsifakos
